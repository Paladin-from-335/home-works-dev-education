package com.github.balls1;

import javax.swing.*;
import java.awt.*;

public class BallsFrame extends JFrame {

    public BallsPanel ballsPanel = new BallsPanel(this);

    public BallsFrame() throws HeadlessException {
        setLayout(null);
        setSize(900, 750);
        ballsPanel.setBounds(10, 10, 700, 700);
        ballsPanel.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(5.0f)));
        add(ballsPanel);
        setVisible(Boolean.TRUE);
    }
}
