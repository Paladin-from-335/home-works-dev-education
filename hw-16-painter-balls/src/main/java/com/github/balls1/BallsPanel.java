package com.github.balls1;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class BallsPanel extends JPanel {

    public BallsPanel(BallsFrame ballsFrame){
        setLayout(null);
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                Ball ball = new Ball(e.getPoint(),ballsFrame.ballsPanel);
                add(ball);
                Thread t = new Thread(ball);
                t.start();

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        setVisible(Boolean.TRUE);
    }
}
