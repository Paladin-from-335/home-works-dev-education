package com.github.balls1;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Ball extends JPanel implements Runnable {

    private Random rnd = new Random();

    private int dx;

    private int dy;

    private int ballSize = rnd.nextInt((100 - 1) +1) +1;

   private float r = rnd.nextFloat();

   private float g = rnd.nextFloat();

   private float b = rnd.nextFloat();

    private Color clr;

    private Point point;

    public Ball(Point point, JPanel panel) {
        this.clr = new Color(r, g, b);
        this.dx = this.rnd.nextInt((20 - 5) + 1) - 5;
        this.dy = this.rnd.nextInt((20 - 5) + 1) - 5;
        if ((ballSize + point.x) >= panel.getWidth()) {
            point.x = point.x - ballSize;
        }
        if ((ballSize + point.y) >= panel.getHeight()) {
            point.y = point.y - ballSize;
        }
        this.point = point;
        setSize(ballSize, ballSize);
        setOpaque(false);
    }

        private void move() {
      JPanel pan = (JPanel) getParent();
      if(this.point.x <= 0 || this.point.x + ballSize >= pan.getWidth()) {
            dx = -dx;
        }
        if(this.point.y <= 0 || this.point.y + ballSize >= pan.getHeight()) {
            dy = -dy;
        }
        this.point.translate(dx,dy);
        setLocation(point);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics g2d = (Graphics) g;
        g2d.setColor(clr);
        g2d.fillOval(1,1,ballSize,ballSize);
    }

    @Override
    public void run() {
        try {

            while (true) {
                move();

                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().isInterrupted();
        }
    }
}
