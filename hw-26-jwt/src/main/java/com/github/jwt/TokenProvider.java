package com.github.jwt;


import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;

import java.security.SecureRandom;
import java.text.ParseException;


public class TokenProvider {

    public static void main(String[] args) throws ParseException, JOSEException {
        generateToken();
    }

    public static JWSObject generateToken() throws JOSEException, ParseException {
        String payload = "Hello, world!";
        SecureRandom random = new SecureRandom();
        byte[] sharedSecret = new byte[32];
        random.nextBytes(sharedSecret);
        JWSSigner signer = new MACSigner(sharedSecret);
        JWSObject jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.HS256), new Payload(payload));
        jwsObject.sign(signer);
        String token = jwsObject.serialize();
        jwsObject = JWSObject.parse(token);
        return jwsObject;
    }


    public static boolean isValidToken(JWSObject jwsObject, String payload, byte[] sharedSecret) throws JOSEException {
        JWSVerifier verifier = new MACVerifier(sharedSecret);
        System.out.println(jwsObject.verify(verifier) && payload.equals(jwsObject.getPayload().toString()));
        return jwsObject.verify(verifier) && payload.equals(jwsObject.getPayload().toString());
    }
}
