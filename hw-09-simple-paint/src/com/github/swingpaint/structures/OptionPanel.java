package com.github.swingpaint.structures;

import com.github.swingpaint.dialogs.LoadDialog;
import com.github.swingpaint.dialogs.SaveDialog;
import com.github.swingpaint.utils.FileAnalyzer;
import com.github.swingpaint.utils.FileCreator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class OptionPanel extends JPanel {
    public OptionPanel(PaintPanel paintPanel) {
        setBounds(0, 0, 800, 100);
        setBackground(new Color(173, 170, 213));
        setLayout(null);

        JButton colorButton = new JButton("Color");
        colorButton.setBounds(20, 30, 100, 40);

        colorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                paintPanel.setColor(JColorChooser.showDialog(new JFrame(), "Select a color", Color.RED));
            }
        });

        JButton thicknessButton = new JButton("Thickness");
        thicknessButton.setBounds(140, 30, 100, 40);

        thicknessButton.addActionListener(new ActionListener() {
            Integer[] variants = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

            @Override
            public void actionPerformed(ActionEvent e) {
                int k = (int) JOptionPane.showInputDialog(new JFrame(), "Thickness : ", "Thickness", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(), variants, 1);
                paintPanel.setThickness(k);
            }
        });

        JButton clearButton = new JButton("Clear");
        clearButton.setBounds(260, 30, 100, 40);

        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                paintPanel.clear();
            }
        });

        JButton saveButton = new JButton("Save");
        saveButton.setBounds(600, 10, 100, 40);

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    SaveDialog sd = new SaveDialog();
                    if (sd.isNormalEnded()) {
                        FileCreator fileCreator = new FileCreator(sd.getFileExtension(), sd.getFileName(), paintPanel.getLines());
                    }
                } catch (IOException exception) {
                    JOptionPane.showMessageDialog(new JFrame(), "Try again", "Something is wrong", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        JButton loadButton = new JButton("Load");
        loadButton.setBounds(600, 50, 100, 40);

        loadButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    LoadDialog loadDialog = new LoadDialog();
                    FileAnalyzer fileAnalyzer = new FileAnalyzer(loadDialog.getFile());
                    paintPanel.repaintLines(fileAnalyzer.getLines());
                } catch (IOException exception) {
                    JOptionPane.showMessageDialog(new JFrame(), "Something wrong with file", "Something is wrong", JOptionPane.ERROR_MESSAGE);
                }
            }

        });

        add(colorButton);
        add(thicknessButton);
        add(clearButton);
        add(saveButton);
        add(loadButton);

        setVisible(true);
    }
}
