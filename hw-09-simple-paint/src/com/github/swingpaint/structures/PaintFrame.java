package com.github.swingpaint.structures;

import javax.swing.*;
import java.awt.*;

public class PaintFrame extends JFrame {
    public PaintFrame(PaintPanel panel, OptionPanel optionPanel){
        setTitle("Simple Paint");
        setBounds(200,100,800,700);
        setBackground(Color.BLACK);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        add(panel);
        add(optionPanel);

        setVisible(Boolean.TRUE);
    }
}