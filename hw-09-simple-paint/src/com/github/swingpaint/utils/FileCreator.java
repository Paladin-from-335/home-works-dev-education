package com.github.swingpaint.utils;

import com.github.swingpaint.dialogs.LoadDialog;
import com.github.swingpaint.formats.*;
import com.github.swingpaint.models.Line;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileCreator {

    public FileCreator(Extension extension, String fileName, List<Line> lines) throws IOException {

        String fileSeparator = System.getProperty("file.separator");
        String relativePath = fileSeparator + "Files";
        String resultFileName = relativePath + fileName + extension.getValue();
        File file = new File(resultFileName);
        FileWriter fileWriter = new FileWriter(file);
        String inputString = "";
        switch (extension) {
            case JSON: {
                JsonFormat jsonFormat = new JsonFormat();
                inputString = jsonFormat.toFormat(lines);
            }
            break;
            case YML: {
                YmlFormat ymlFormat = new YmlFormat();
                inputString = ymlFormat.toFormat(lines);
            }
            break;
            case BIN: {
                BinFormat binFormat = new BinFormat();
                inputString = binFormat.toFormat(lines);
            }
            break;
            case XML: {
                XmlFormat xmlFormat = new XmlFormat();
                inputString = xmlFormat.toFormat(lines);
            }
            break;
            case CSV: {
                CsvFormat csvFormat = new CsvFormat();
                inputString = csvFormat.toFormat(lines);
            }
            break;
        }
        fileWriter.write(inputString);
        fileWriter.close();
    }

}
