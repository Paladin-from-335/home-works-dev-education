package com.github.swingpaint.formats;

import com.github.swingpaint.models.Line;

import java.util.List;

public interface IFormat {
    String toFormat(List<Line> lines);
    List<Line> fromFormat(String input);
}
