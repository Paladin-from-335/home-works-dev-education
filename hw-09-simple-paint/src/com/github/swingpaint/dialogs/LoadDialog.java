package com.github.swingpaint.dialogs;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class LoadDialog {

    private final String[] extensions = {"json","csv","yml","xml","bin"};

    private FileNameExtensionFilter filter = new FileNameExtensionFilter("",extensions);

    private File file;

    public LoadDialog(){

            String fileSeparator = System.getProperty("file.separator");
            String relativePath = fileSeparator + "Files";
            JFileChooser jFileChooser = new JFileChooser(relativePath);
            jFileChooser.setFileFilter(filter);
            jFileChooser.showOpenDialog(new JFrame());
            file = jFileChooser.getSelectedFile();

    }

    public File getFile(){
        return this.file;
    }
}
