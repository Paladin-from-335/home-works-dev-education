package com.github.swingpaint;

import com.github.swingpaint.structures.OptionPanel;
import com.github.swingpaint.structures.PaintFrame;
import com.github.swingpaint.structures.PaintPanel;

public class Main {
    public static void main(String[] args) {
        PaintPanel panel = new PaintPanel();
        OptionPanel optionPanel = new OptionPanel(panel);
        PaintFrame frame = new PaintFrame(panel, optionPanel);
    }
}