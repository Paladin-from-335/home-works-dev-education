package com.github.log4j.tasks;

import org.apache.log4j.Logger;

import java.util.Random;

public class RandomException {
    private final Random random = new Random();
    private int randomNum;
    private static final Logger LOGGER = Logger.getLogger(RandomException.class);

    public int getRandomNum() {

        try {
            randomNum = random.nextInt(10);
            if (randomNum > 5) {
                LOGGER.info("Приложение успешно запущено");
                return randomNum;
            }
            throw new LessThanFive("Genereated number less than or equal to 5. Your number: " + randomNum);
        } catch (LessThanFive ex) {
            LOGGER.error(ex.getMessage());
            return randomNum;
        }
    }
}


