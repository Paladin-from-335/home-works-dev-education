package com.github.log4j.tasks;


import org.apache.log4j.Logger;

public class CustomException {

   private static final Logger LOGGER = Logger.getLogger(CustomException.class);

    public CustomException() {
        LOGGER.warn("Dividing by zero");

        try {
            int a = 1/0;
        } catch (Throwable cause) {
            LOGGER.error("Can't divide by zero", cause);
        }
    }
}
