package com.github.log4j.tasks;

public class LessThanFive extends Exception {
    public LessThanFive(String errorMsg) {
        super(errorMsg);
    }
}
