function inputFirstBox(){
    var firstnumber = parseFloat(document.getElementById("convertFrom").value);
    if(firstnumber == null || firstnumber == NaN){
            document.getElementById("convertTo").value = "";
        }
    var numberConverted = calculateToDefault(firstnumber);
    document.getElementById("convertTo").value = numberConverted;
}

function inputSecondBox(){
    var firstnumber = parseFloat(document.getElementById("convertTo").value);
    if(firstnumber == null){
        document.getElementById("convertFrom").value = "";
    }
    document.getElementById("convertFrom").value = firstnumber;
}

function dropDownMenuSelector(){
    var arrayTime = ["Seconds", "Minutes", "Hour", "Day", "Week", "Month", "Astronomical year"];
    var arrayLength = ["Meters", "Kilometers", "Mile", "Nautical mile", "Cable", "League", "Foot", "Yard"];
    var arrayWeight = ["Gram", "Kilogram", "Carat", "Eng pound", "Pound", "Stone", "Rus pound"];
    var arrayVolume = ["M^3", "Liter", "Gallon", "Pint", "Quart", "Barrel", "Cubic foot", "Cubic inch"];
    var arrayTemperature = ["Celsius", "Kelvin", "Reaumur", "Romer", "Rankine", "Newton", "Delisle", "Fahrenheit"];
    var selectSelector1 = document.getElementById("typeSelector1");
    var selectSelector2 = document.getElementById("typeSelector2");
    var item = document.getElementById("typeSelector");
    var selectedItem = item.options[item.selectedIndex].text;
    switch (selectedItem){
        case "Time":
            selectSelector1.options.length = 0;
            selectSelector2.options.length = 0;
            for(var i = 0; i < arrayTime.length; i++){
                selectSelector1.options[selectSelector1.options.length] = new Option(arrayTime[i]);
                selectSelector2.options[selectSelector2.options.length] = new Option(arrayTime[i]);
            }
            break;
        case "Length":
            selectSelector1.options.length = 0;
            selectSelector2.options.length = 0;
            for(var i = 0; i < arrayTime.length; i++){
                selectSelector1.options[selectSelector1.options.length] = new Option(arrayLength[i]);
                selectSelector2.options[selectSelector2.options.length] = new Option(arrayLength[i]);
            }
            break;
        case "Weight":
            selectSelector1.options.length = 0;
            selectSelector2.options.length = 0;
            for(var i = 0; i < arrayTime.length; i++){
                selectSelector1.options[selectSelector1.options.length] = new Option(arrayWeight[i]);
                selectSelector2.options[selectSelector2.options.length] = new Option(arrayWeight[i]);
            }
            break;
        case "Volume":
            selectSelector1.options.length = 0;
            selectSelector2.options.length = 0;
            for(var i = 0; i < arrayTime.length; i++){
                selectSelector1.options[selectSelector1.options.length] = new Option(arrayVolume[i]);
                selectSelector2.options[selectSelector2.options.length] = new Option(arrayVolume[i]);
            }
            break;
        case "Temperature":
            selectSelector1.options.length = 0;
            selectSelector2.options.length = 0;
            for(var i = 0; i < arrayTime.length; i++){
                selectSelector1.options[selectSelector1.options.length] = new Option(arrayTemperature[i]);
                selectSelector2.options[selectSelector2.options.length] = new Option(arrayTemperature[i]);
            }
            break;
        default:
            break;
    }
}

function calculateToDefault(numberOne){
    var typeSelectorOne = document.getElementById("typeSelector1");
    var selectedItemOnOne = typeSelectorOne.options[typeSelectorOne.selectedIndex].text;
    var typeSelectorTwo = document.getElementById("typeSelector2");
    var selectedItemOnTwo = typeSelectorTwo.options[typeSelectorTwo.selectedIndex].text;
    var inputNumber = numberOne;
    var convertedNum = 0;
    var resultNum = 0;

    switch (selectedItemOnOne){
                case "Seconds":
                    convertedNum = inputNumber;
                    break;
                case "Minutes":
                    convertedNum = inputNumber * 60;
                    break;
                case "Hour":
                    convertedNum = inputNumber * 3600;
                    break;
                case "Day":
                    convertedNum = inputNumber * 86400;
                    break;
                case "Week":
                    convertedNum = inputNumber * 604800;
                    break;
                case "Month":
                    convertedNum = inputNumber * (2.628 * Math.pow(10, 6));
                    break;
                case "Astronomical year":
                    convertedNum = inputNumber * (3.154 * Math.pow(10, 7));
                    break;
                case "Gram":
                    convertedNum = inputNumber;
                    break;
                case "Kilogram":
                    convertedNum = inputNumber * 1000;
                    break;
                case "Carat":
                    convertedNum = inputNumber / 5;
                    break;
                case "Eng pound":
                case "Pound":
                    convertedNum = inputNumber * 453.5923;
                    break;
                case "Stone":
                    convertedNum = inputNumber * 6350;
                    break;
                case "Rus pound":
                    convertedNum = inputNumber * 409.5171;
                    break;
                case "Liter":
                    convertedNum = inputNumber;
                    break;
                case "M^3":
                    convertedNum = inputNumber * 1000;
                    break;
                case "Gallon":
                    convertedNum = inputNumber * 3.785;
                    break;
                case "Pint":
                    convertedNum = inputNumber / 2.1133764189;
                    break;
                case "Quart":
                    convertedNum = inputNumber / 1.057;
                    break;
                case "Barrel":
                    convertedNum = inputNumber * 117;
                    break;
                case "Cubic foot":
                    convertedNum = inputNumber * 28.317;
                    break;
                case "Cubic inch":
                    convertedNum = inputNumber / 61.024;
                    break;
                case "Celsius":
                    convertedNum = inputNumber;
                    break;
                case "Kelvin":
                    convertedNum = inputNumber - 273.15;
                    break;
                case "Fahrenheit":
                    convertedNum = (inputNumber - 32) * 0.55555555555555555555555555555556;
                    break;
                case "Reaumur":
                    convertedNum = inputNumber / 0.8;
                    break;
                case "Romer":
                    convertedNum = (inputNumber - 7.5) / 0.525;
                    break;
                case "Rankine":
                    convertedNum = (inputNumber - 491.67) * 0.55555555555555555555555555555556;
                    break;
                case "Newton":
                    convertedNum = inputNumber / 0.33;
                    break;
                case "Delisle":
                    convertedNum = (inputNumber + 100) / 1.5;
                    break;
                case "Meters":
                    convertedNum = inputNumber;
                    break;
                case "Kilometers":
                    convertedNum = inputNumber * 1000;
                    break;
                case "Mile":
                    convertedNum = inputNumber * 1609;
                    break;
                case "Nautical mile":
                    convertedNum = inputNumber * 1852;
                    break;
                case "Cable":
                    convertedNum = inputNumber * 185.2;
                    break;
                case "League":
                    convertedNum = inputNumber * 5556;
                    break;
                case "Foot":
                    convertedNum = inputNumber / 3.28084;
                    break;
                case "Yard":
                    convertedNum = inputNumber / 1.094;
                    break;
    }

    switch(selectedItemOnTwo){
            case "Seconds":
                resultNum = convertedNum;
                break;
            case "Minutes":
                resultNum = convertedNum / 60;
                break;
            case "Hour":
                resultNum = convertedNum / 3600;
                break;
            case "Day":
                resultNum = convertedNum / 86400;
                break;
            case "Week":
                resultNum = convertedNum / 604800;
                break;
            case "Month":
                resultNum = convertedNum / (2.628 * Math.pow(10, 6));
                break;
            case "Astronomical year":
                resultNum = convertedNum / (3.154 * Math.pow(10, 7));
                break;
            case "Gram":
                resultNum = convertedNum;
                break;
            case "Kilogram":
                resultNum = convertedNum / 1000;
                break;
            case "Carat":
                resultNum = convertedNum * 5;
                break;
            case "Eng pound":
            case "Pound":
                resultNum = convertedNum / 453.5923;
                break;
            case "Stone":
                resultNum = convertedNum / 6350;
                break;
            case "Rus pound":
                resultNum = convertedNum / 409.5171;
                break;
            case "Liter":
                resultNum = convertedNum;
                break;
            case "M^3":
                resultNum = convertedNum / 1000;
                break;
            case "Gallon":
                resultNum = convertedNum / 3.785;
                break;
            case "Pint":
                resultNum = convertedNum * 2.1133764189;
                break;
            case "Quart":
                resultNum = convertedNum * 1.057;
                break;
            case "Barrel":
                resultNum = convertedNum / 117;
                break;
            case "Cubic foot":
                resultNum = convertedNum / 28.317;
                break;
            case "Cubic inch":
                resultNum = convertedNum * 61.024;
                break;
            case "Meters":
                resultNum = convertedNum;
                break;
            case "Kilometers":
                resultNum = convertedNum / 1000;
                break;
            case "Mile":
                resultNum = convertedNum / 1609;
                break;
            case "Nautical mile":
                resultNum = convertedNum / 1852;
                break;
            case "Cable":
                resultNum = convertedNum / 185.2;
                break;
            case "League":
                resultNum = convertedNum / 5556;
                break;
            case "Foot":
                resultNum = convertedNum * 3.28084;
                break;
            case "Yard":
                resultNum = convertedNum * 1.094;
                break;
            case "Celsius":
                resultNum = convertedNum;
                break;
            case "Kelvin":
                resultNum = convertedNum + 273.15;
                break;
            case "Fahrenheit":
                resultNum = convertedNum * 1.8 + 32;
                break;
            case "Reaumur":
                resultNum = convertedNum * 0.8;
                break;
            case "Romer":
                resultNum = convertedNum * 0.525 + 7.5;
                break;
            case "Rankine":
                resultNum = convertedNum * 1.8 + 32 + 459.67;
                break;
            case "Newton":
                resultNum = convertedNum * 0.33;
                break;
            case "Delisle":
                resultNum = (100 - convertedNum) * 1.5;
                break;
            default:
                break;
            }
    return resultNum;
}

function clearValue(){
    document.getElementById("convertFrom").value = "";
    document.getElementById("convertTo").value = "";
}