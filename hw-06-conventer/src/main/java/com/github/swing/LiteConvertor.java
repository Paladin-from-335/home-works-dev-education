package com.github.swing;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;

public class LiteConvertor {
    private JPanel contentPane;
    private JTextField textField1;
    private JTextField textField2;
    private JComboBox<String> comboBox1;
    private JComboBox<String> comboBox2;
    private JComboBox<String> comboBox3;
    private JButton resetButton;
    String switchCase;
    String[] stringOfLength = {"Meters", "Kilometers", "Mile", "Nautical mile", "Cable", "League", "Foot", "Yard"};
    String[] stringOfWeight = {"Gram", "Kilogram", "Carat", "Eng pound", "Pound", "Stone", "Rus pound"};
    String[] stringOfTemperature = {"Celsius", "Kelvin", "Reaumur", "Romer", "Rankine", "Newton", "Delisle", "Fahrenheit"};
    String[] stringOfVolume = {"M^3", "Liter", "Gallon", "Pint", "Quart", "Barrel", "Cubic foot", "Cubic inch"};
    String[] stringOfTime = {"Seconds", "Minutes", "Hour", "Day", "Week", "Month", "Astronomical year"};


    public LiteConvertor() {
        DocumentListener listenerBox1 = new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                double number = Double.parseDouble(textField1.getText());
                String valueOfBox1 = String.valueOf(comboBox1.getSelectedItem());
                String valueOfBox2 = String.valueOf(comboBox2.getSelectedItem());
                String valueOFBox3 = String.valueOf(comboBox3.getSelectedItem());
                textField2.setText(Double.toString(SelectConvertion.typeSelector(valueOfBox1, number, valueOfBox2, valueOFBox3)));
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                try {
                    double number = Double.parseDouble(textField1.getText());
                    String valueOfBox1 = String.valueOf(comboBox1.getSelectedItem());
                    String valueOfBox2 = String.valueOf(comboBox2.getSelectedItem());
                    String valueOFBox3 = String.valueOf(comboBox3.getSelectedItem());
                    textField2.setText(Double.toString(SelectConvertion.typeSelector(valueOfBox1, number, valueOfBox2, valueOFBox3)));
                } catch (NumberFormatException Ignore) {
                    textField2.setText(null);
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }
        };

        textField1.getDocument().addDocumentListener(listenerBox1);

        comboBox3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchCase = String.valueOf(comboBox3.getSelectedItem());
                switch (switchCase) {
                    case "Length":
                        comboBox1.removeAllItems();
                        comboBox2.removeAllItems();
                        for (int i = 0; i < stringOfLength.length; i++) {
                            comboBox1.addItem(stringOfLength[i]);
                            comboBox2.addItem(stringOfLength[i]);
                        }
                        break;
                    case "Weight":
                        comboBox1.removeAllItems();
                        comboBox2.removeAllItems();
                        for (int i = 0; i < stringOfWeight.length; i++) {
                            comboBox1.addItem(stringOfWeight[i]);
                            comboBox2.addItem(stringOfWeight[i]);
                        }
                        break;
                    case "Temperature":
                        comboBox1.removeAllItems();
                        comboBox2.removeAllItems();
                        for (int i = 0; i < stringOfTemperature.length; i++) {
                            comboBox1.addItem(stringOfTemperature[i]);
                            comboBox2.addItem(stringOfTemperature[i]);
                        }
                        break;
                    case "Volume":
                        comboBox1.removeAllItems();
                        comboBox2.removeAllItems();
                        for (int i = 0; i < stringOfVolume.length; i++) {
                            comboBox1.addItem(stringOfVolume[i]);
                            comboBox2.addItem(stringOfVolume[i]);
                        }
                        break;
                    case "Time":
                        comboBox1.removeAllItems();
                        comboBox2.removeAllItems();
                        for (int i = 0; i < stringOfTime.length; i++) {
                            comboBox1.addItem(stringOfTime[i]);
                            comboBox2.addItem(stringOfTime[i]);
                        }
                        break;
                    default:
                        textField1.setText("No text for u");
                        break;
                }
            }
        });

        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textField1.setText(null);
                textField2.setText(null);
            }
        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("Calculator");
        frame.setContentPane(new LiteConvertor().contentPane);
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.pack();
        frame.setVisible(true);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(10, 10, 10, 10), -1, -1));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(5, 3, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        panel1.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer2 = new com.intellij.uiDesigner.core.Spacer();
        panel1.add(spacer2, new com.intellij.uiDesigner.core.GridConstraints(4, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        comboBox3 = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
        defaultComboBoxModel1.addElement("Length");
        defaultComboBoxModel1.addElement("Temperature");
        defaultComboBoxModel1.addElement("Weight");
        defaultComboBoxModel1.addElement("Time");
        defaultComboBoxModel1.addElement("Volume");
        comboBox3.setModel(defaultComboBoxModel1);
        panel1.add(comboBox3, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, 30), new Dimension(150, 30), new Dimension(150, 30), 0, false));
        resetButton = new JButton();
        resetButton.setText("Clear values");
        panel1.add(resetButton, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, 30), new Dimension(150, 30), new Dimension(150, 30), 0, false));
        comboBox1 = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel2 = new DefaultComboBoxModel();
        comboBox1.setModel(defaultComboBoxModel2);
        panel1.add(comboBox1, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, 30), new Dimension(150, 30), new Dimension(150, 30), 0, false));
        textField1 = new JTextField();
        textField1.setText("");
        panel1.add(textField1, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, 30), new Dimension(150, 30), new Dimension(150, 30), 0, false));
        comboBox2 = new JComboBox();
        panel1.add(comboBox2, new com.intellij.uiDesigner.core.GridConstraints(3, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, 30), new Dimension(150, 30), new Dimension(150, 30), 0, false));
        textField2 = new JTextField();
        panel1.add(textField2, new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, 30), new Dimension(150, 30), new Dimension(150, 30), 0, false));
        final JLabel label1 = new JLabel();
        label1.setAlignmentX(1.0f);
        label1.setAlignmentY(1.0f);
        label1.setText("Convert from");
        panel1.add(label1, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, 20), new Dimension(150, 20), new Dimension(150, 20), 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Convert to");
        panel1.add(label2, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, 20), new Dimension(150, 20), new Dimension(150, 20), 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }

}
