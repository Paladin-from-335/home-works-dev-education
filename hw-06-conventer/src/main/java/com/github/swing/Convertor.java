package com.github.swing;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Locale;

public class Convertor {

    private JTextField textField1;
    private JTextField textField2;
    private JPanel mainField;
    private JButton RESETButton;
    private JButton lengthButton;
    private JButton temperatureButton;
    private JButton weightButton;
    private JButton timeButton;
    private JButton volumeButton;
    private JButton button5;
    private JButton button4;
    private JButton button3;
    private JButton button2;
    private JButton button1;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JLabel label1;
    private JLabel label2;
    String switchCase;
    String[] weight = {"kg", "g", "carat", "eng pound", "pound", "stone", "rus pound"};
    String[] time = {"sec", "min", "hour", "day", "week", "month", "year"};
    String[] temperature = {"C", "K", "F", "Re", "Ro", "Ra", "N", "D"};
    String[] length = {"m", "km", "mile", "nautrical mile", "cable", "league", "foot", "yard"};
    String[] volume = {"l", "m^3", "gallon", "pint", "quart", "barrel", "cubic foot", "cubic inch"};

    public Convertor() {
        RESETButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textField2.setText(null);
                textField1.setText(null);
            }
        });

        lengthButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label1.setText("Заданная величина, " + length[0]);
                button1.setText("m to Km");
                button2.setText("m to Mile");
                button3.setText("m to nautical mile");
                button4.setText("m to cable");
                button5.setText("m to league");
                button6.setText("m to foot");
                button7.setText("m to yard");
                button1.setVisible(true);
                button2.setVisible(true);
                button3.setVisible(true);
                button4.setVisible(true);
                button5.setVisible(true);
                button6.setVisible(true);
                button7.setVisible(true);
                switchCase = "length";
            }
        });

        temperatureButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label1.setText("Заданная величина, " + temperature[0]);
                button1.setText("C to K");
                button2.setText("C to F");
                button3.setText("C to Re");
                button4.setText("C to Ro");
                button5.setText("C to Ra");
                button6.setText("C to N");
                button7.setText("C to D");
                button1.setVisible(true);
                button2.setVisible(true);
                button3.setVisible(true);
                button4.setVisible(true);
                button5.setVisible(true);
                button6.setVisible(true);
                button7.setVisible(true);
                switchCase = "temperature";
            }
        });

        weightButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label1.setText("Заданная величина, " + weight[0]);
                button1.setText("kg to g");
                button2.setText("kg to carat");
                button3.setText("kg to eng pound");
                button4.setText("kg to stone");
                button5.setText("kg to rus pound");
                button6.setText("kg to pound");
                button1.setVisible(true);
                button2.setVisible(true);
                button3.setVisible(true);
                button4.setVisible(true);
                button5.setVisible(true);
                button6.setVisible(true);
                button7.setVisible(false);
                switchCase = "weight";
            }
        });

        timeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label1.setText("Заданная величина, " + time[0]);
                button1.setText("sec to min");
                button2.setText("sec to hour");
                button3.setText("sec to day");
                button4.setText("sec to week");
                button5.setText("sec to month");
                button6.setText("sec to year");
                button1.setVisible(true);
                button2.setVisible(true);
                button3.setVisible(true);
                button4.setVisible(true);
                button5.setVisible(true);
                button6.setVisible(true);
                button7.setVisible(false);
                switchCase = "time";
            }
        });

        button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showInternalMessageDialog(null, "- Кнопками \"Weight\", \"Length\", \"Time\", \"Temperature\", \"Volume\"\nвыбирается режим конвертирования"
                        + "\n- Кнопками, к примеру \" kg to carat\", заданная величина конвертируется из\nкиллограмов в караты"
                        + "\n- Конпка \"RESET\" делает отчистку в полях ввода");
            }
        });
        volumeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label1.setText("Заданная величина, " + volume[0]);
                button1.setText("l to m^3");
                button2.setText("l to gallon");
                button3.setText("l to pint");
                button4.setText("l to quart");
                button5.setText("l to barrel");
                button6.setText("l to cubic foot");
                button7.setText("l to cubic inch");
                button1.setVisible(true);
                button2.setVisible(true);
                button3.setVisible(true);
                button4.setVisible(true);
                button5.setVisible(true);
                button6.setVisible(true);
                button7.setVisible(true);
                switchCase = "volume";
            }
        });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number / 1000;
                        label2.setText("Конвертированная, " + volume[1]);
                        break;
                    case "length":
                        number = number / 1000;
                        label2.setText("Конвертированная, " + length[1]);
                        break;
                    case "weight":
                        number = number * 1000;
                        label2.setText("Конвертированная, " + weight[1]);
                        break;
                    case "time":
                        number = number / 60;
                        label2.setText("Конвертированная, " + time[1]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[1]);
                        number = number + 273.15;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number / 3.785;
                        label2.setText("Конвертированная, " + volume[2]);
                        break;
                    case "length":
                        number = number / 1609;
                        label2.setText("Конвертированная, " + length[2]);
                        break;
                    case "weight":
                        number = number * 5000;
                        label2.setText("Конвертированная, " + weight[2]);
                        break;
                    case "time":
                        number = number / 3600;
                        label2.setText("Конвертированная, " + time[2]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[2]);
                        number = number * 1.8 + 32;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number * 2.133;
                        label2.setText("Конвертированная, " + volume[3]);
                        break;
                    case "length":
                        number = number / 1852;
                        label2.setText("Конвертированная, " + length[3]);
                        break;
                    case "weight":
                        number = number * 2.205;
                        label2.setText("Конвертированная, " + weight[3]);
                        break;
                    case "time":
                        number = number / 86400;
                        label2.setText("Конвертированная, " + time[3]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[3]);
                        number = number * 0.8;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number * 1.057;
                        label2.setText("Конвертированная, " + volume[4]);
                        break;
                    case "length":
                        number = number / 185.3184;
                        label2.setText("Конвертированная, " + length[4]);
                        break;
                    case "weight":
                        number = number / 6.35;
                        label2.setText("Конвертированная, " + weight[4]);
                        break;
                    case "time":
                        number = number / 604800;
                        label2.setText("Конвертированная, " + time[4]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[4]);
                        number = number * 0.525 + 7.5;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number / 117;
                        label2.setText("Конвертированная, " + volume[5]);
                        break;
                    case "length":
                        number = number / 5556;
                        label2.setText("Конвертированная, " + length[5]);
                        break;
                    case "weight":
                        number = number / 0.45359237;
                        label2.setText("Конвертированная, " + weight[5]);
                        break;
                    case "time":
                        number = number / (2.628 * Math.pow(10, 6));
                        label2.setText("Конвертированная, " + time[5]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[5]);
                        number = number * 1.8 + 491.67;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number / 28.317;
                        label2.setText("Конвертированная, " + volume[6]);
                        break;
                    case "length":
                        number = number * 3.218;
                        label2.setText("Конвертированная, " + length[6]);
                        break;
                    case "weight":
                        number = number * 2.205;
                        label2.setText("Конвертированная, " + weight[6]);
                        break;
                    case "time":
                        number = number / (3.154 * Math.pow(10, 7));
                        label2.setText("Конвертированная, " + time[6]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[6]);
                        number = number * 0.33;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });
        button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number * 61.024;
                        label2.setText("Конвертированная, " + volume[7]);
                        break;
                    case "length":
                        number = number * 1.094;
                        label2.setText("Конвертированная, " + length[7]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[7]);
                        number = (100 - number) * 1.5;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });
    }

    public static void main(String[] args) throws IOException {
        JFrame frame = new JFrame("Convertor");
        frame.setContentPane(new Convertor().mainField);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.pack();
        frame.setVisible(true);
    }
}
