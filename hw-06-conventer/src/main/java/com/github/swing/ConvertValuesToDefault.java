package com.github.swing;

public class ConvertValuesToDefault {

    public static double numberConvertorLength(String valueOfBox1, double number, String valueOfBox2){

        // Convertor to default(meter) value
        double convertedNumber = 0.0;
        double fromValueToDefault = 0.0;
        switch (valueOfBox1) {
            case "Meters":
                fromValueToDefault = number;
                break;
            case "Kilometers":
                fromValueToDefault = number * 1000;
                break;
            case "Mile":
                fromValueToDefault = number * 1609;
                break;
            case "Nautical mile":
                fromValueToDefault = number * 1852;
                break;
            case "Cable":
                fromValueToDefault = number * 185.2;
                break;
            case "League":
                fromValueToDefault = number * 5556;
                break;
            case "Foot":
                fromValueToDefault = number / 3.28084;
                break;
            case "Yard":
                fromValueToDefault = number / 1.094;
                break;
            default:
                break;
        }
        convertedNumber = FromDefaultToSelectedItem.convertToValueLength(valueOfBox2, fromValueToDefault);
        return convertedNumber;
    }

    public static double numberConvertorTemperature(String valueOfBox1, double number, String valueOfBox2) {

        // Convertor to default(celsius) value
        double convertedNumber = 0.0;
        double fromValueToDefault = 0.0;
        switch (valueOfBox1) {
            case "Celsius":
                fromValueToDefault = number;
                break;
            case "Kelvin":
                fromValueToDefault = number - 273.15;
                break;
            case "Fahrenheit":
                fromValueToDefault = (number - 32) * 0.55555555555555555555555555555556;
                break;
            case "Reaumur":
                fromValueToDefault = number / 0.8;
                break;
            case "Romer":
                fromValueToDefault = (number - 7.5) / 0.525;
                break;
            case "Rankine":
                fromValueToDefault = (number - 491.67) * 0.55555555555555555555555555555556;
                break;
            case "Newton":
                fromValueToDefault = number / 0.33;
                break;
            case "Delisle":
                fromValueToDefault = (number + 100) / 1.5;
                break;
            default:
                break;
        }
        convertedNumber = FromDefaultToSelectedItem.convertToValueTemperature(valueOfBox2, fromValueToDefault);
        return convertedNumber;
    }

    public static double numberConvertorVolume(String valueOfBox1, double number, String valueOfBox2) {

        // Convertor to default(liter) value
        double convertedNumber = 0.0;
        double fromValueToDefault = 0.0;
        switch (valueOfBox1) {
            case "Liter":
                fromValueToDefault = number;
                break;
            case "M^3":
                fromValueToDefault = number * 1000;
                break;
            case "Gallon":
                fromValueToDefault = number * 3.785;
                break;
            case "Pint":
                fromValueToDefault = number / 2.1133764189;
                break;
            case "Quart":
                fromValueToDefault = number / 1.057;
                break;
            case "Barrel":
                fromValueToDefault = number * 117;
                break;
            case "Cubic foot":
                fromValueToDefault = number * 28.317;
                break;
            case "Cubic inch":
                fromValueToDefault = number / 61.024;
                break;
            default:
                break;
        }
        convertedNumber = FromDefaultToSelectedItem.convertToValueVolume(valueOfBox2, fromValueToDefault);
        return convertedNumber;
    }

    public static double numberConvertorWeight(String valueOfBox1, double number, String valueOfBox2) {

        // Convertor to default(gram) value
        double convertedNumber = 0.0;
        double fromValueToDefault = 0.0;
        switch (valueOfBox1) {
            case "Gram":
                fromValueToDefault = number;
                break;
            case "Kilogram":
                fromValueToDefault = number * 1000;
                break;
            case "Carat":
                fromValueToDefault = number / 5;
                break;
            case "Eng pound":
            case "Pound":
                fromValueToDefault = number * 453.5923;
                break;
            case "Stone":
                fromValueToDefault = number * 6350;
                break;
            case "Rus pound":
                fromValueToDefault = number * 409.5171;
                break;
            default:
                break;
        }
        convertedNumber = FromDefaultToSelectedItem.convertToValueWeight(valueOfBox2, fromValueToDefault);
        return convertedNumber;
    }

    public static double numberConvertorTime(String valueOfBox1, double number, String valueOfBox2) {

        // Convertor to default(seconds) value
        double convertedNumber = 0.0;
        double fromValueToDefault = 0.0;
        switch (valueOfBox1) {
            case "Seconds":
                fromValueToDefault = number;
                break;
            case "Minutes":
                fromValueToDefault = number * 60;
                break;
            case "Hour":
                fromValueToDefault = number * 3600;
                break;
            case "Day":
                fromValueToDefault = number * 86400;
                break;
            case "Week":
                fromValueToDefault = number * 604800;
                break;
            case "Month":
                fromValueToDefault = number * (2.628 * Math.pow(10, 6));
                break;
            case "Astronomical year":
                fromValueToDefault = number * (3.154 * Math.pow(10, 7));
                break;
            default:
                break;
        }
        convertedNumber = FromDefaultToSelectedItem.convertToValueTime(valueOfBox2, fromValueToDefault);
        return convertedNumber;
    }
}
