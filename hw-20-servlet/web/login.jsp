<%--
  Created by IntelliJ IDEA.
  User: goods
  Date: 01.05.2021
  Time: 00:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Auth</title>
</head>
<style>
    .wrapper {
        position: relative;
        width: 500px;
        height: 500px;
        border: 1px solid black;
        margin-top: 100px;
        margin-left: auto;
        margin-right: auto;
        display: flex;
        flex-direction: column;
    }
</style>
<body>
<div class="wrapper">
    <h3>Authentication</h3>
    <form action="AuthServlet" method="get">
        <input type="text" class="login" placeholder="Enter username">
        <input type="password" class="password" placeholder="Password">
        <button class="button">Confirm</button>
    </form>
</div>
</body>
</html>
