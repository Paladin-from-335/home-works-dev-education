package com.github.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(
        name = "LoginServlet",
        description = "Servlet",
        urlPatterns = {"/login"}
)
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Enter doGet");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        resp.setContentType("text/html");

        if(AuthHandler.isAllowed(login, password)){
            resp.setStatus(200);
            HttpSession session = req.getSession();
            session.setAttribute("user", "Lorem");
            session.setMaxInactiveInterval(1800);
            Cookie userName = new Cookie("user", login);
            userName.setMaxAge(1800);
            resp.addCookie(userName);
            RequestDispatcher dispatcher =req.getRequestDispatcher("/good");
            dispatcher.forward(req, resp);
        } else {
            resp.setStatus(403);
            RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/login.jsp");
            PrintWriter out = resp.getWriter();
            out.println("<h2> Username or password wrong. Status code: " + resp.getStatus() + " </h2>");
            requestDispatcher.include(req, resp);
        }
    }
}
