package com.github.servlet;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;

public class Main {
    public static void main(String[] args) throws ServletException, LifecycleException {
        Tomcat tomcat = new Tomcat();
        String port = System.getenv("PORT");
        if(port == null || port.isEmpty()){
            port = "8081";
        }
        tomcat.setPort(Integer.parseInt(port));
        tomcat.addWebapp("", "C:\\Users\\goods\\Desktop\\Projects\\home-works-dev-education\\hw-20-servlet\\web");
        tomcat.start();
        tomcat.getServer().await();
    }
}
