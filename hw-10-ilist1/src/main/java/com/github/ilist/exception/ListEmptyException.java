package com.github.ilist.exception;

public class ListEmptyException extends RuntimeException {

    public ListEmptyException() {
    }

    public ListEmptyException(String message) {
        super(message);
    }
}
