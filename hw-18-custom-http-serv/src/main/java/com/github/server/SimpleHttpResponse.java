package com.github.server;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class SimpleHttpResponse {

    private Socket socket;

    private SimpleCustomHandler simpleCustomHandler;

    public SimpleHttpResponse(Socket socket, SimpleCustomHandler simpleCustomHandler) {
        this.simpleCustomHandler = simpleCustomHandler;
        this.socket = socket;
        this.makeResponse(simpleCustomHandler.getResponse(), simpleCustomHandler.getHtmlPage());
    }

    private void makeResponse(String response, String html) {
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))) {
            System.out.println(response+html);
            writer.write(response);
            writer.write(html);
        } catch (IOException e){
            e.printStackTrace();
        }
    }


}
