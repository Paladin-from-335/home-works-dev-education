package com.github.server;

public class SimpleCustomHandler {

    private final SimpleHttpRequest simpleHttpRequest;

    public SimpleCustomHandler(SimpleHttpRequest simpleHttpRequest) {
        this.simpleHttpRequest = simpleHttpRequest;
    }

    public String getResponse() {
        return "HTTP/1.1 200 OK\n\r" +
                String.format("Content-Length: %d\n\r", getHtmlPage().length()) +
                "Content-Type: text/html\n\r\n";
    }

    public String getHtmlPage() {

        return  "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n\r" +
                "<html>\n\r" +
                "<head>" +
                "<title> Server response </title>" +
                "</head>\n" +
                "<body>\n\r" +
                "<h1>" +
                "Hey man(or woman)! <br>" +
                "Req type: " + this.simpleHttpRequest.getType() + "<br>\n\r" +
                "</h1>\n\r" +
                "</body>\n\r" +
                "</html>\n\r\n\r";
    }
}

