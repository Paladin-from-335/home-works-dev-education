SELECT COUNT(id) FROM People_list;

SELECT AVG age FROM People_list;

SELECT DISTINCT last_name
FROM People_list ORDER BY last_name ASC;

SELECT last_name, COUNT(*) FROM People_list
GROUP BY last_name HAVING COUNT(*)>1;

SELECT * FROM People_list WHERE last_name LIKE '%б%';

SELECT * FROM person WHERE id_street IS NULL

SELECT People_list.first_name, People_list.last_name, People_list.age, Street_list.name
FROM People_list LEFT JOIN Street_list
ON People_list.id_street = Street_list.id
WHERE age < 18 AND Street_list.name = 'проспект Правды';

SELECT DISTINCT name, COUNT(*) FROM Street_list
INNER JOIN People_list ON Street_list.id = People_list.id_street
GROUP BY name HAVING COUNT(*)>0 ORDER BY name ASC;

SELECT * FROM Street_list WHERE CHAR_LENGTH(name) = 6;

SELECT DISTINCT name, COUNT(*) FROM Street_list
INNER JOIN People_list ON Street_list.id = People_list.id_street
GROUP BY name HAVING COUNT(*)>3 ORDER BY name ASC;
