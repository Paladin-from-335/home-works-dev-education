package com.github.calc;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Throwable thrown) {
            thrown.printStackTrace();
        }
        SimpleCalc abt = new SimpleCalc(new ActionManager());
        abt.setVisible(true);
    }

}
