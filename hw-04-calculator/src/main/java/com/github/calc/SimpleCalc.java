package com.github.calc;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;

public class SimpleCalc extends JFrame {

    private JTextField tfFirstNum;

    private JTextField tfOperation;

    private JTextField tfSecondNum;

    private JTextField tfResult;

    public SimpleCalc(ActionManager ac) {
        super("Калькулятор");
        JPanel content = new JPanel();
        content.setLayout(null);
        content.setBackground(new Color(221,226,206));

        JLabel lblFirstNum = new JLabel("Число 1");
        lblFirstNum.setHorizontalAlignment(SwingConstants.RIGHT);
        lblFirstNum.setBounds(5, 10, 85, 21);


        JLabel lblSecondNum = new JLabel("Число 2");
        lblSecondNum.setBounds(5, 70, 85, 21);
        lblSecondNum.setHorizontalAlignment(SwingConstants.RIGHT);

        JLabel lblOperation = new JLabel("Операция");
        lblOperation.setBounds(5, 125, 85, 21);
        lblOperation.setHorizontalAlignment(SwingConstants.RIGHT);

        JLabel lblResult = new JLabel("Результат");
        lblResult.setBounds(5, 260, 85, 21);
        lblResult.setHorizontalAlignment(SwingConstants.RIGHT);


        this.tfFirstNum = new JTextField(20);
        tfFirstNum.setBounds(100, 5, 200, 40);

        this.tfSecondNum = new JTextField(20);
        tfSecondNum.setBounds(100, 60, 200, 40);

        this.tfOperation = new JTextField(20);
        tfOperation.setBounds(100, 115, 200, 40);

        this.tfResult = new JTextField(20);
        tfResult.setBounds(100, 250, 200, 40);

        JButton btnCalc = new JButton();
        try {
            Image img = ImageIO.read(getClass().getResource("/com/github/calc/assets/button.png"));
            btnCalc.setIcon(new ImageIcon(img));
        }catch (Exception ex){
            System.out.println(ex);
        }
        btnCalc.setBounds(50, 180, 250, 49);
        btnCalc.setBackground(new Color(221,226,206));

        ac.setTfFirstNum(this.tfFirstNum);
        ac.setTfSecondNum(this.tfSecondNum);
        ac.setTfOperation(this.tfOperation);
        ac.setTfResult(this.tfResult);

        btnCalc.addActionListener(ac.getBtnEqActionListener());

        content.add(lblFirstNum);
        content.add(lblOperation);
        content.add(lblSecondNum);
        content.add(tfFirstNum);
        content.add(tfOperation);
        content.add(tfSecondNum);
        content.add(btnCalc);

        content.add(lblResult);
        content.add(tfResult);

        setSize(350, 450);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setContentPane(content);
        setResizable(false);
    }
}