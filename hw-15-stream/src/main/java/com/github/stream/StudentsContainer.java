package com.github.stream;

import com.github.stream.entity.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StudentsContainer {

    private List<Student> students = new ArrayList<>();

    public StudentsContainer(List<Student> students) {
        init(students);
    }

    public void init(List<Student> students) {
        this.students.clear();
        students.forEach(student -> this.students.add(new Student(student)));
    }

    public List<Student> getStudents() {
        return students;
    }

    public List<Student> byFaculty(String faculty) {
        return this.students
                .stream()
                .filter(stud -> stud.getFaculty().equals(faculty))
                .collect(Collectors.toList());
    }

    public List<Student> byFacultyAndCourse(String faculty, String course) {
        return this.students
                .stream()
                .filter(stud -> stud.getFaculty().equals(faculty))
                .filter(stud -> stud.getCourse().equals(course))
                .collect(Collectors.toList());
    }

    public List<Student> bornAfter(int year) {

        return this.students
                .stream()
                .filter(stud -> stud.getYearOfBirth() > year)
                .collect(Collectors.toList());
    }

    public List<Student> bornAfterFirst(int year) {

        return this.students
                .stream()
                .filter(stud -> stud.getYearOfBirth() > year)
                .findFirst()
                .stream().collect(Collectors.toList());

    }

    public List<String> groupList(String group) {
        return this.students
                .stream()
                .filter(stud -> stud.getGroup().equals(group))
                .flatMap(stud -> Stream.of(stud.getFirstName() + " " + stud.getLastName()))
                .collect(Collectors.toList());
    }

    public long studentsOnFaculty(String faculty) {
        return this.students
                .stream()
                .filter(stud -> stud.getFaculty().equals(faculty))
                .count();
    }

    public List<Student> changeFaculty(String fromFactulty, String toFaculty) {
        return this.students
                .stream()
                .filter(stud -> stud.getFaculty().equals(fromFactulty))
                .peek(stud -> stud.setFaculty(toFaculty))
                .collect(Collectors.toList());
    }

    public List<Student> changeGroup(String fromGroup, String toGroup) {
        return this.students
                .stream()
                .filter(stud -> stud.getFaculty().equals(fromGroup))
                .peek(stud -> stud.setFaculty(toGroup))
                .collect(Collectors.toList());
    }

    public String studentsToString(){
        return this.students
                .stream()
                .flatMap(stud -> Stream.of(stud.getFirstName() + " " + stud.getLastName() + " - "
                        + stud.getFaculty() + " " + stud.getGroup() + "\n"))
                .reduce("", (partialString, element) -> partialString + element);

    }

    public Map<String, List<Student>> groupByFaculty(){
        return this.students
                .stream()
                .collect(Collectors.groupingBy(Student::getFaculty));
    }

    public Map<String, List<Student>> groupByCourse(){
        return this.students
                .stream()
                .collect(Collectors.groupingBy(Student::getCourse));
    }

    public Map<String, List<Student>> groupByGroup(){
        return this.students
                .stream()
                .collect(Collectors.groupingBy(Student::getGroup));
    }

    public boolean checkAllFaculty(String faculty){
        return this.students
                .stream()
                .allMatch(stud -> stud.getFaculty().equals(faculty));
    }

    public boolean checkOneFaculty(String faculty){
        return this.students
                .stream()
                .anyMatch(stud -> stud.getFaculty().equals(faculty));
    }

    public boolean checkAllGroup(String group){
        return this.students
                .stream()
                .allMatch(stud -> stud.getGroup().equals(group));
    }

    public boolean checkOneGroup(String group){
        return this.students
                .stream()
                .anyMatch(stud -> stud.getGroup().equals(group));
    }

}
