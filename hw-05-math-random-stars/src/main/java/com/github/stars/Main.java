package com.github.stars;

public class Main {
    public static void main(String[] args) {
        Figures.figureToOutSquare();
        System.out.println("=========================");
        Figures.figureToOutEmptySquare();
        System.out.println("=========================");
        Figures.figureToOutTriangleTopLeft();
        System.out.println("=========================");
        Figures.figureToOutTriangleBottomLeft();
        System.out.println("=========================");
        Figures.figureToOutTriangleBottomRight();
        System.out.println("=========================");
        Figures.figureToOutTriangleTopRight();
        System.out.println("=========================");
        Figures.figureToOutX();
        System.out.println("=========================");
        Figures.figureToOutTriangleFromTopToCentre();
        System.out.println("=========================");
        Figures.figureToOutTriangleFromBottomToCentre();
        System.out.println("=========================");
    }
}