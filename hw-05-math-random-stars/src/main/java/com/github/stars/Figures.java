package com.github.stars;

public class Figures {
    public static void figureToOutSquare(){
        String[][] twoDimArray = new String[7][7];
        for (int i = 0; i< twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                twoDimArray[i][j] = "*";
            }
        }
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                System.out.print(" " + twoDimArray[i][j] + " ");
            }
            System.out.println();
        }
    }
    public static void figureToOutEmptySquare(){
        String[][] twoDimArray = new String[7][7];
        for (int i = 0; i< twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                if(i == 0 || i == 6){
                    twoDimArray[i][j] = "*";
                } else if(i != 0 || i != 6) {
                    if (j == 0 || j == 6) {
                        twoDimArray[i][j] = "*";
                    } else if (j > 0 && j < 6) {
                        twoDimArray[i][j] = " ";
                    }
                }
            }
        }
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                System.out.print(" " + twoDimArray[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void figureToOutTriangleTopLeft(){
        String[][] twoDimArray = new String[7][7];
        for (int i = 0; i< twoDimArray.length; i++) {
            for(int j = 0; j < twoDimArray[0].length; j++){
                if (i == 0) {
                    twoDimArray[i][j] = "*";
                } else if(i > 0){
                    if(j == 0 || j == twoDimArray.length - i - 1){
                        twoDimArray[i][j] = "*";
                    } else {
                        twoDimArray[i][j] = " ";
                    }
                }
            }
        }

        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                System.out.print(" " + twoDimArray[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void figureToOutTriangleBottomLeft(){
        String[][] twoDimArray = new String[7][7];
        for (int i = 0; i< twoDimArray.length; i++) {
            for(int j = 0; j < twoDimArray[0].length; j++){
                if (i == 6) {
                    twoDimArray[i][j] = "*";
                } else if(i < 6){
                    if(j == 0 || j == i){
                        twoDimArray[i][j] = "*";
                    } else {
                        twoDimArray[i][j] = " ";
                    }
                }
            }
        }

        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                System.out.print(" " + twoDimArray[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void figureToOutTriangleBottomRight(){
        String[][] twoDimArray = new String[7][7];
        for (int i = 0; i< twoDimArray.length; i++) {
            for(int j = 0; j < twoDimArray[0].length; j++){
                if (i == 6) {
                    twoDimArray[i][j] = "*";
                } else if(i < 6){
                    if(j == 6 || j == twoDimArray.length - i - 1){
                        twoDimArray[i][j] = "*";
                    } else {
                        twoDimArray[i][j] = " ";
                    }
                }
            }
        }

        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                System.out.print(" " + twoDimArray[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void figureToOutTriangleTopRight(){
        String[][] twoDimArray = new String[7][7];
        for (int i = 0; i< twoDimArray.length; i++) {
            for(int j = 0; j < twoDimArray[0].length; j++){
                if (i == 0) {
                    twoDimArray[i][j] = "*";
                } else if(i > 0){
                    if(j == 6 || j == i){
                        twoDimArray[i][j] = "*";
                    } else {
                        twoDimArray[i][j] = " ";
                    }
                }
            }
        }

        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                System.out.print(" " + twoDimArray[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void figureToOutX(){
        String[][] twoDimArray = new String[7][7];
        for (int i = 0; i< twoDimArray.length; i++) {
            for(int j = 0; j < twoDimArray[0].length; j++){
                if (i == j || i == (twoDimArray.length - j - 1)) {
                    twoDimArray[i][j] = "*";
                } else if(i != j){
                    twoDimArray[i][j] = " ";
                }
            }
        }

        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                System.out.print(" " + twoDimArray[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void figureToOutTriangleFromTopToCentre(){
        String[][] twoDimArray = new String[7][7];
        for (int i = 0; i < twoDimArray.length; i++) {
            for(int j = 0; j < twoDimArray[0].length; j++){
                if (i >= 0 && i < (twoDimArray.length/2 + 1)) {
                    if(i == 0){
                        twoDimArray[i][j] = "*";
                    } else if(i == j || i == (twoDimArray.length - j - 1)){
                        twoDimArray[i][j] = "*";
                    } else if(i != j){
                        twoDimArray[i][j] = " ";
                    }
                } else {
                    twoDimArray[i][j] = " ";
                }
            }
        }

        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                System.out.print(" " + twoDimArray[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void figureToOutTriangleFromBottomToCentre() {
        String[][] twoDimArray = new String[7][7];
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                if (i >= (twoDimArray.length / 2) && i <= twoDimArray.length) {
                    if (i == 6) {
                        twoDimArray[i][j] = "*";
                    } else if (i == j || i == (twoDimArray.length - j - 1)) {
                        twoDimArray[i][j] = "*";
                    } else if (i != j) {
                        twoDimArray[i][j] = " ";
                    }
                } else {
                    twoDimArray[i][j] = " ";
                }
            }
        }

        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                System.out.print(" " + twoDimArray[i][j] + " ");
            }
            System.out.println();
        }
    }
}
