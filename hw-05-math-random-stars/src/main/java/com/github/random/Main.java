package com.github.random;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write task's num from 1 to 4:");
        String comm = scanner.nextLine();
        switch (comm){
            case"1":
                System.out.println("Random number:");
                Tasks.Rand();
            break;
            case"2":
                System.out.println("Ten random numbers:");
                Tasks.TenRand();
                break;
          case"3":
              System.out.println("Ten random numbers from 0 to 10:");
                Tasks.RandomRange();
                break;
          case"4":
              System.out.println("Random range of random numbers: ");
            Tasks.RandomAmountRange();
                break;
            default:
                System.out.println("Тебе было сказано ввести число от 1 до 4. \n Теперь запускай заново.");
                break;
        }
    }
}
