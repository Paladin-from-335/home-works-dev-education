package com.github.math;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write task's num from 1 to 4:");
        String comm = scanner.nextLine();
        switch (comm) {
            case "1":
                System.out.println("Range of shot:");
                Tasks.Howitzer();
                break;
            case "2":
                System.out.println("Distance between cars: ");
                Tasks.Aoutos();
                break;
            case "3":
                System.out.println("Dots: ");
                Tasks.Dots();
                break;
            case "4":
                System.out.println("Calculating: ");
                Tasks.Calculating();
                break;
            default:
                System.out.println("Тебе было сказано ввести число от 1 до 4. \n Теперь запускай заново.");
                break;
        }
    }
}
