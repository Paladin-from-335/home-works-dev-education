package com.github.transform;

import org.junit.Assert;
import org.junit.Test;

public class TestClass {
    @Test
    public void NumToString(){
        String exp = "10";
        String act = Main.Number(10);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void DoubToString(){
        String exp = "23.859";
        String act = Main.DoubNum(23.859);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void StringToInt(){
        int exp =  257;
        int act = Main.ParseInt("257");
        Assert.assertEquals(exp, act);
    }    @Test
    public void StringToDoub(){
        double delta = 0;
        double exp =  257.257;
        double act = Main.ParseDoub("257.257");
        Assert.assertEquals(exp, act, delta );
    }
}
