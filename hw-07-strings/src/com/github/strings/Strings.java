package com.github.strings;

import java.util.Arrays;

public class Strings {

    public static String ShortWord(String s ) {
        s = s.replaceAll("\\pP" + " ", " ");
        System.out.println(s);
        String[] splited = s.split("\\s");
        System.out.println(Arrays.toString(splited));
        int minLen = splited[0].length();
        String minWord = "";
        for (int i = 0; i < splited.length; i++) {
            if (splited[i].length() < minLen) {
                minLen = splited[i].length();
                minWord = splited[i];
            }
        }
        return minWord;
    }

    public static String[] SignToDol(String[] words, int length){
        if(words == null){
            throw new NullPointerException ("No words");
        }
        for(int i = 0; i < words.length; i++){
            if(words[i].length() == length){
                words[i] = words[i].replaceAll(".{3}$", "\\$\\$\\$");
            }
        }
        return words;
    }

    public static String addSpace(String inp) {
        inp = inp.replaceAll("(?<=\\p{Punct})(?=\\w)", " ");
        return inp;
    }

    public static String DeleteDuplicates(String inp){
      String res = "";
      for (int i = 0; i< inp.length(); i++){
           if(!res.contains(String.valueOf(inp.charAt(i)))){
               res += String.valueOf(inp.charAt(i));
          }
      }
      return res;
    }
    public static int WordsCounter(String inp){
        inp = inp.replaceAll("(?<=\\p{Punct})(?=\\w)", " ");
        inp = inp.replaceAll("\\pP", " ");
        inp = inp.replaceAll("( )+", " ");
        String [] arr = inp.split(" ");
        return arr.length;
    }


    public static String DeletePart(String inp, int len, int point) {
        String delete = "";
        for (int i = point; i <= len; i++) {
             delete = inp.substring(inp.indexOf(" "));
        }
        return delete;
    }

    public static String Reverse(String inp){
        return new StringBuilder(inp).reverse().toString();
    }
    public static String DeleteLast(String inp){
        inp = new StringBuilder(inp).reverse().toString();
        String fword = inp.substring(inp.indexOf(" "));
        inp = new StringBuilder(fword).reverse().toString();
  return inp;
    }
}
