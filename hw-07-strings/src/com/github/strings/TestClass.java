package com.github.strings;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class TestClass {
    @Test
    public void Shorter(){
String exp = "ab";
String act = Strings.ShortWord("Aaaaaa, aaaaaaaaaa; aaaaaa! Blet. Baobab, ab. ");
        Assert.assertEquals(exp, act);
    }
    @Test
    public  void SignToDol(){
        String[] exp = {"a$$$", "b$$$", "lol","kek","brbrbr", "c$$$"};
        String[] act = Strings.SignToDol(new String[]{"aaaa", "bbbb", "lol", "kek", "brbrbr", "cccc"}, 4);
        Assert.assertArrayEquals(exp, act);
    }
    @Test
    public  void  addSpace(){
        String exp = "A@ b, c, d! ads; f, p: o! Sho? Mda!";
        String act = Strings.addSpace("A@b, c,d!ads;f, p:o! Sho?Mda!");
        Assert.assertEquals(exp, act);
    }
    @Test
    public  void DeleteDupl(){
        String exp = "Aa,bcdz";
        String act = Strings.DeleteDuplicates("Aaaaaaa,bbb,b,b,c,c,c,c,d,z,a");
        Assert.assertEquals(exp,act);
    }
    @Test
    public void WordsCounter(){
        int exp = 10;
        int act = Strings.WordsCounter("Aaa, bb   , e, q!a@eee, p? F:,l;k");
        Assert.assertEquals(exp, act);
    }
    @Test
    public  void DeletePart(){
String exp = " ipsum dolor sit amet";
String act = Strings.DeletePart("Lorem ipsum dolor sit amet", 5, 0);
Assert.assertEquals(exp, act);
    }
    @Test
    public void Reverse(){
        String exp = "telb amebO";
        String act = Strings.Reverse("Obema blet");

    }
    @Test
    public void DeleteLast(){
        String exp = "Lorem ipsum dolor sit" + " ";
        String act = Strings.DeleteLast("Lorem ipsum dolor sit amet");
        Assert.assertEquals(exp,act);
    }
}
