package com.github.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SimpleHttpRequest {

    private final Map<String, String> header;
    public SimpleHttpRequest(SocketChannel client) throws IOException {
        this.header = parseRequest(getRequest(client));
    }

    public String getRequestType(){
        return this.header.get("TypeRequest:");
    }

    public String getBody() {
        return this.header.get("Body:");
    }

    public String getRequest(SocketChannel client) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        String request = "";
        int count = 0;
        while ((count = client.read(buffer)) > -1) {
            System.out.println(count);
            byte[] v = buffer.array();
            request = request + new String(v);
            System.out.println("--1--" + request);
            buffer.clear();
            if(count < 1024){
                break;
            }
        }
        return request;
    }

    public static Map<String, String> parseRequest(String request) throws IOException {
        String[] k_v = new String[2];
        Map<String, String> header = new HashMap<>();
        List<String> splitRequest = Stream.of(request.split("\n"))
                .map (elem -> new String(elem))
                .collect(Collectors.toList());
        int count = 0;
        for(String str:splitRequest) {
            count++;
            if (count == 1) {
                k_v = str.split(" ", 2);
                header.put("TypeRequest:", k_v[0]);
            } else {
                if(str != null && !str.isEmpty() && !str.equals("\r")){
                    k_v = str.split(":", 2);
                    header.put(k_v[0], k_v[1]);

                }
                if (k_v[0].equals("Content-Length") && str.equals("\r")) {
                    int index = Integer.parseInt(k_v[1].trim());
                    String body = splitRequest.get(splitRequest.size()-1).substring(0,index);
                    header.put("Body:", body);
                }
                if(str.equals("\r")){
                    break;
                }
            }
        }
        System.out.println(request);
        return header;
    }
}

