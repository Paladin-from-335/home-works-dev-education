package com.github.trees.impl;

import com.github.trees.ITrees;

import java.util.Objects;

public class RecursiveAVLTree implements ITrees {

    private Node root;

    private static class Node {

        int val;

        int height;

        Node right;

        Node left;

        public Node(int val) {
            this.val = val;
            height = 0;
        }
    }

    int height(Node node) {
        if (node == null){
            return -1;
        } else {
            return node.height;
        }
    }


    public Node rotateWithLeft(Node c) {
        Node p;

        p = c.left;
        c.left = p.right;
        p.right = c;

        c.height = Math.max(height(c.left), height(c.right)) + 1;
        p.height = Math.max(height(p.left), height(p.right)) + 1;

        return p;
    }


    public Node rotateWithRight(Node c) {
        Node p;

        p = c.right;
        c.right = p.left;
        p.left = c;

        c.height = Math.max(height(c.left), height(c.right)) + 1;
        p.height = Math.max(height(p.left), height(p.right)) + 1;

        return p;
    }


    public Node doubleRotateWithLeft(Node c) {
        Node tmp;

        c.left = rotateWithRight(c.left);
        tmp = rotateWithLeft(c);

        return tmp;
    }

    public Node doubleRotateWithRight(Node c) {
        Node tmp;

        c.right = rotateWithLeft(c.right);
        tmp = rotateWithRight(c);

        return tmp;
    }

    @Override
    public void init(int[] init) {
        if (Objects.isNull(init)) {
            init = new int[0];
        }
        for (int elem : init) {
            add(elem);
        }
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public void add(int val) {
        Node newNode = new Node(val);
        if(this.root == null) {
            this.root = newNode;
        } else {
            this.root = addNode(newNode, this.root);
        }
    }

    public Node addNode(Node newNode, Node par) {
        Node newpar = par;
        if (newNode.val < par.val) {
            if (par.left == null) {
                par.left = newNode;
            } else {
                par.left = addNode(newNode, par.left);
                if ((height(par.left) - height(par.right)) == 2) {
                    if (newNode.val < par.left.val) {
                        newpar = rotateWithLeft(par);
                    } else {
                        newpar = doubleRotateWithLeft(par);
                    }
                }
            }
        } else if (newNode.val > par.val) {
            if (par.right == null) {
                par.right = newNode;
            } else {
                par.right = addNode(newNode, par.right);
                if ((height(par.right) - height(par.left)) == 2) {
                    if (newNode.val > par.right.val) {
                        newpar = rotateWithRight(par);
                    } else {
                        newpar = doubleRotateWithRight(par);
                    }
                }
            }
        }
        if ((par.left == null) && (par.right != null)){
            par.height = par.right.height + 1;
        } else if ((par.right == null) && (par.left != null)){
            par.height = par.left.height + 1;
        } else {
            par.height = Math.max(height(par.left), height(par.right)) + 1;
        }
        return newpar;
    }

    @Override
    public int size() {
        return sizeNode(this.root);
    }

    public int sizeNode(Node p) {
        if (p == null) {
            return 0;
        }
        int count = 0;
        count += sizeNode(p.right);
        count++;
        count += sizeNode(p.left);
        return count;
    }

    @Override
    public int leaves() {
        return countNodeLeaves(this.root);
    }
    public int countNodeLeaves(Node node){
        if(node == null) {
            return 0;
        }
        if(node.left == null && node.right == null){
            return 1;
        } else {
            return countNodeLeaves(node.left) + countNodeLeaves(node.right);
        }
    }

    @Override
    public int nodes() {
        return countNodes(this.root);
    }

    public int countNodes(Node node) {
        if (node == null){
            return 0;
        } else {
            return 1 + countNodes(node.left) + countNodes(node.right);
        }
    }

    @Override
    public int getHeight() {
        if (this.root == null)
            return -1;
        int leftHeight = height(root.left);
        int rightHeight = height(root.right);

        return Math.max(leftHeight, rightHeight) + 1;
    }

    @Override
    public int getWidth() {
        int width;
        int maxWidth = 0;
        int height = getHeight();

        for (int i = 0; i <= height; i++){
            width = getWidth(this.root, i);
            if(width > maxWidth){
                maxWidth = width;
            }
        }
        return maxWidth;
    }

    private int getWidth(Node node, int level) {
        if (node == null){
            return 0;
        }
        if(level == 1){
            return 1;
        } else if (level > 1) {
            return getWidth(node.right, level-1)
                    + getWidth(node.left, level-1);
        } else {
            return 0;
        }
    }

    @Override
    public void reverse() {
        reverse(this.root);
    }
    private void reverse(Node node){
        if(node == null){
            return;
        }
        Node tmp = node.left;
        node.left = node.right;
        node.right = tmp;

        reverse(node.left);
        reverse(node.right);
    }

    @Override
    public void delete(int val) {
        this.root = delete(this.root, val);
    }

    private Node minValueNode(Node node) {
        Node current = node;

        while (current.left != null)
            current = current.left;
        return current;
    }

    private Node delete(Node root, int val){
    if (root == null)
            return root;

        if ( val < root.val )
    root.left = delete(root.left, val);

        else if( val > root.val )
    root.right = delete(root.right, val);

        else {
            if ((root.left == null) || (root.right == null)) {

                Node temp;
                if (root.left != null)
                    temp = root.left;
                else
                    temp = root.right;

                if (temp == null) {
                    temp = root;
                    root = null;
                } else {
                    root = temp;
                }
                temp = null;
            } else {
                Node temp = minValueNode(root.right);
                root.val = temp.val;

                root.right = delete(root.right, temp.val);
            }
        }

        if (root == null)
            return root;

    root.height = Math.max(height(root.left), height(root.right)) + 1;

    int balance = getBalance(root);

        if (balance > 1 && getBalance(root.left) >= 0)
            return rightRotate(root);

        if (balance > 1 && getBalance(root.left) < 0) {
        root.left =  leftRotate(root.left);
        return rightRotate(root);
    }

        if (balance < -1 && getBalance(root.right) <= 0)
            return leftRotate(root);

        if (balance < -1 && getBalance(root.right) > 0) {
        root.right = rightRotate(root.right);
        return leftRotate(root);
    }

        return root;
}
    private Node leftRotate(Node x) {
        Node y = x.right;
        Node T2 = y.left;

        y.left = x;
        x.right = T2;

        x.height = Math.max(height(x.left), height(x.right))+1;
        y.height = Math.max(height(y.left), height(y.right))+1;

        return y;
    }
    private Node rightRotate(Node y) {
        Node x = y.left;
        Node T2 = x.right;

        x.right = y;
        y.left = T2;

        y.height = Math.max(height(y.left), height(y.right))+1;
        x.height = Math.max(height(x.left), height(x.right))+1;

        return x;
    }

    private int getBalance(Node N) {
        if (N == null)
            return 0;
        return height(N.left) - height(N.right);
    }

    @Override
    public int[] toArray() {
        if (Objects.isNull(this.root)) {
            return new int[0];
        }
        int[] array = new int[size()];
        Counter c = new Counter();
        toArrayNode(array, c, this.root);
        return array;
    }

    @Override
    public void print() {
        inOrder(this.root);
    }

    public void inOrder(Node node) {
        if (node == null) {
            return;
        }
        inOrder(node.left);
        System.out.printf("%s ", node.val);
        inOrder(node.right);
    }

    public void toArrayNode(int[] array, Counter c, Node node) {
        if (Objects.isNull(node)) {
            return;
        }
        toArrayNode(array, c, node.left);
        array[c.index++] = node.val;
        toArrayNode(array, c, node.right);
    }

    private static class Counter {
        int index = 0;
    }
}
