package com.github.trees.impl;

import com.github.trees.ITrees;


import java.util.Objects;

public class RecursiveBinaryTree implements ITrees {

    private Node root;

    private class Node {
        int val;
        Node leftNode;
        Node rightNode;

        public Node(int val) {
            this.val = val;
        }

    }

    @Override
    public void init(int[] array) {
        this.clear();
        for (int element : array) {
            this.add(element);
        }
    }

    @Override
    public void print() {
        print(this.root);
    }

    public void print(Node node) {
        if (node == null) {
            return;
        }
        print(node.leftNode);
        System.out.printf("%s ", node.val);
        print(node.rightNode);
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public int size() {
        return size(this.root);
    }

    private int size(Node node) {
        if (node == null) {
            return 0;
        } else {
            return size(node.leftNode) + 1 + size(node.rightNode);
        }
    }

    @Override
    public int[] toArray() {
        if (Objects.isNull(this.root)) {
            return new int[0];
        }
        int[] array = new int[size()];
        Counter c = new Counter();
        toArrayNode(array, c, this.root);
        return array;
    }

    public void toArrayNode(int[] array, Counter c, Node node) {
        if (Objects.isNull(node)) {
            return;
        }
        toArrayNode(array, c, node.leftNode);
        array[c.index++] = node.val;
        toArrayNode(array, c, node.rightNode);
    }

    private static class Counter {
        int index = 0;
    }


    @Override
    public void add(int val) {
        if (this.root == null) {
            this.root = new Node(val);
        } else {
            this.root = add(this.root, val);
        }

    }

    private Node add(Node node, int value) {
        if (node == null) {
            node = new Node(value);
        } else {
            if (value <= node.val) {
                node.leftNode = add(node.leftNode, value);
            } else {
                node.rightNode = add(node.rightNode, value);
            }
        }
        return node;
    }


    @Override
    public void delete(int val) {
        this.root = delete(this.root, val);
    }

    private Node delete(Node node, int val) {
        if (node == null) {
            return null;
        }
        if (node.val < val) {
            node.leftNode = delete(node.leftNode, val);
        } else if (node.val > val) {
            node.rightNode = delete(node.rightNode, val);
        } else {
            if (node.rightNode != null && node.leftNode != null) {
                Node minRightNode = minNode(node.rightNode);
                node.val = minRightNode.val;
                node.rightNode = delete(node.rightNode, minRightNode.val);
            } else  if(node.rightNode != null){
                node = node.rightNode;
            } else if(node.leftNode != null){
                node = node.leftNode;
            } else {
                node = null;
            }
        }
        return node;
    }
    public Node minNode(Node node){
        if(node.leftNode == null){
            return node;
        } else {
            return minNode(node.leftNode);
        }
    }

    @Override
    public int getHeight() {
        return getHeight(this.root);
    }
    private int getHeight(Node node){
        if(node == null){
            return 0;
        } else{
           int leftHeight = getHeight(node.leftNode);
           int rightHeight = getHeight(node.rightNode);

           return Math.max(leftHeight, rightHeight) +1;
        }
    }

    @Override
    public int getWidth() {
        int width;
        int maxWidth = 0;
        int height = getHeight();

        for (int i = 0; i <= height; i++){
            width = getWidth(this.root, i);
            if(width > maxWidth){
                maxWidth = width;
            }
        }
        return maxWidth;
    }

    private int getWidth(Node node, int level) {
        if (node == null){
            return 0;
        }
        if(level == 1){
            return 1;
        } else if (level > 1) {
            return getWidth(node.rightNode, level-1)
                    + getWidth(node.leftNode, level-1);
        } else {
            return 0;
        }
    }


    @Override
    public int nodes() {
        return nodes(this.root);
    }

    public int nodes(Node node) {
        if (node == null){
            return 0;
        } else {
            return 1 + nodes(node.leftNode) + nodes(node.rightNode);
        }
    }

    @Override
    public int leaves() {
        return leaves(this.root);
    }
    public int leaves(Node node){
        if(node == null) {
            return 0;
        }
        if(node.leftNode == null && node.rightNode == null){
            return 1;
        } else {
            return leaves(node.leftNode) + leaves(node.rightNode);
        }
    }

    @Override
    public void reverse() {
        reverse(this.root);
    }
    private void reverse(Node node){
        if(node == null){
            return;
        }
        Node tmp = node.leftNode;
        node.leftNode = node.rightNode;
        node.rightNode = tmp;

        reverse(node.leftNode);
        reverse(node.rightNode);
    }
}