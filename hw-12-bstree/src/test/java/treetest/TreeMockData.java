package treetest;

public class TreeMockData {

    public static final int[] arrayOfTen = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    public static final int[] arrayOfFive = {1, 2, 3, 4, 5};

    public static final int[] arrayOfFiveNotOrganized = {3, 2, 4, 1, 5};

    public static final int[] arrayOfTenNotOrganized = {10, 4, 2, 1, 5, 6, 9, 7, 8, 3};

    public static final int[] arrayOfOne = {1};

    public static final int[] arrayZero = {};

    public static int[] getArrayOfTen() {
        return arrayOfTen;
    }

    public static int[] getArrayOfFive() {
        return arrayOfFive;
    }

    public static int[] getArrayOfOne() {
        return arrayOfOne;
    }

    public static int[] getArrayOfFiveNotOrganized() {
        return arrayOfFiveNotOrganized;
    }

    public static int[] getArrayOfTenNotOrganized() {
        return arrayOfTenNotOrganized;
    }

    public static int[] getArrayZero() {
        return arrayZero;
    }
}
