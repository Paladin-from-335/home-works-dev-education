package com.github.randomizer;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;


public class Randomizer {
    private final int min;
    private final int max;
    private HashSet set;
    private Random random;
    public Randomizer(int min, int max) {
        this.min = min;
        this.max = max;
        this.set = new HashSet<Integer>();
        this.random = new Random();
    }

    public void generate(){
        if(set.size()==(max-min+1)) {System.out.println("No more available numbers"); System.exit(0);}
        int generated;
        do {generated = random.nextInt(max-min+1) + min;}
        while(!set.add(generated));
        System.out.println("Your number : "+generated);
    }

    public void help(){
        System.out.println("Write 'generate' to generate unique number ");
        System.out.println("Write 'exit' to exit ");
        System.out.println("Write 'help' to see all commands");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Set minimum, more than 1.");
        int min = scanner.nextInt();
        while(min<1||min>500){
            System.out.println("Wrong number, try again");
            min = scanner.nextInt();
        }
        System.out.println("Set maximum, less than 500.");
        int max = scanner.nextInt();
        while(max<1||max>500||max<min){
            System.out.println("Wrong number, try again");
            max = scanner.nextInt();
        }
        Randomizer main = new Randomizer(min, max);
        System.out.println("Write 'generate' to get random number\n Write 'help' for help");
        scanner.reset();
        while(true){
            String line = scanner.next();
            switch(line){
                case("generate"): main.generate();
                    break;
                case("exit"): return;
                case("help"): main.help();
                    break;
                default:
                    System.out.println("Undefined command, try something else");
                    break;
            }
        }
    }
}

